import pytest


def scrub_cookies(response):
    response["headers"]["Cookie"] = "****"
    response["headers"]["Set-Cookie"] = "****"
    return response


@pytest.fixture(scope="module")
def vcr_config():
    return {
        "filter_headers": [("Authorization", "****"), ("API-KEY", "****")],
        "before_record_response": scrub_cookies,
    }


@pytest.fixture(autouse=True)
def define_env(monkeypatch):
    monkeypatch.setenv('ODOO_BASEURL', 'http://odoo-sc.local:8069')
    monkeypatch.setenv('ODOO_APIKEY', 'APIKEY')
